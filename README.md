# Grav - Ansible Role

This role covers deployment, configuration and software updates of Grav. This role is released under MIT Licence and we give no warranty for this piece of software. Currently supported OS - Debian.

The role allows to install grav's modules.

You can deploy test instance using `Vagrantfile` attached to the role.
`vagrant up`

`ansible-playbook -b Playbooks/grav.yml`

Then you can then access your website from your computer on `https://192.168.33.15:443`

You can also add `192.168.33.15 mywebsite.lan` to `/etc/hosts` on your computer. Then you can access your website from your computer on `https://mywebsite.lan`


## Disroot website and Howto
If you want to deploy Disroot website and Howto, go to `Playbooks/grav.yml` and uncomment `#- ../defaults/disroot.yml`.
Then, run the role again `ansible-playbook -b Playbooks/grav.yml`.

Edit `/etc/hosts` on your computer on add:

```
192.168.33.15 disroot.lan
```

You can now access Disroot website from your computer on `https://disroot.lan`.
